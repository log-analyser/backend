//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let rewire = require("rewire");
let notificationUtils = rewire('../utils/notifications.utils');
let nodemailer = require('nodemailer');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
chai.should();

chai.use(chaiHttp);


let notificationMock;

describe('Notifications', () => {

    beforeEach((done) => {
        notificationMock = {
            rulesSets: [{
                variables: [{
                    rules: [{
                        _id: "5af605ebfd143126d5263c7e",
                        startTime: "2018-05-11T20:05:40.622Z",
                        stopTime: "2018-05-10T22:05:40.031Z",
                        regExp: "Warning",
                        value: "100"
                    }],
                    _id: "5af605ebfd143126d5263c7d",
                    name: "K"
                }, {
                    rules: [{
                        _id: "5af605ebfd143126d5263c7c",
                        regExp: "PHP Warning",
                        startTime: "2018-05-11T16:06:09.190Z",
                        stopTime: "2018-05-10T22:06:09.143Z",
                        value: "200"
                    }, {
                        _id: "5af605ebfd143126d5263c7b",
                        regExp: "Warning PHP",
                        startTime: "2018-05-11T16:06:34.400Z",
                        stopTime: "2018-05-11T18:06:34.414Z",
                        value: "300"
                    }],
                    _id: "5af605ebfd143126d5263c7a",
                    name: "L"
                }, {
                    rules: [{
                        _id: "5af6d2e2e15d3440acf90b24",
                        regExp: "^Error"
                    }],
                    _id: "5af6d2e2e15d3440acf90b23",
                    name: "M"
                }],
                _id: "5af605ebfd143126d5263c79",
                name: "Czy jest Warning",
                __v: 0
            }],
            visitedLogs: [{
                "lineNumber": 8,
                "date": new Date("2018-04-24T10:29:00.000Z"),
                "logId": "5afaae0320aa510b7a088437"
            },
            {
                "lineNumber": 41,
                "date": new Date("2018-05-14T23:24:18.000Z"),
                "logId": "5afaae0320aa510b7a088438"
            },
            {
                "lineNumber": 5,
                "date": new Date("2018-04-12T22:09:32.000Z"),
                "logId": "5afaae0320aa510b7a088439"
            },
            {
                "lineNumber": 24,
                "date": new Date("2018-04-12T16:03:41.000Z"),
                "logId": "5afaae0320aa510b7a08843a"
            },
            {
                "lineNumber": 5,
                "date": new Date("2018-04-03T22:09:59.000Z"),
                "logId": "5afaae0320aa510b7a08843b"
            },],
            variables: [{
                K: ["12"],
                L: [],
                M: ["543"]
            }, {
                K: ["321"],
                L: ["432"],
                M: ["543"]
            }, {
                K: ["12"]
            }],
            files: [ {
                    "lineNumber": 2,
                    "_id": "5afaf778e8de5339aa18f52d",
                    "filename": "error.log.6.gz",
                    "path": "~/logs",
                    "line": "[Sat Apr 07 01:48:33.932409 2018] [mpm_prefork:notice] [pid 21918] AH00163: Apache/2.4.25 (Debian) OpenSSL/1.0.2l configured -- resuming normal operations",
                    "variable": {
                        name: "K",
                        value: "321"
                    },
                    "date": new Date("2018-04-06T23:48:33.932Z"),
                },
                {
                    "lineNumber": 29,
                    "_id": "5afaf778e8de5339aa18f52e",
                    "filename": "talaga.log",
                    "path": "~/logs2",
                    "line": "[Sun Apr 01 17:59:20.914307 2018] [mpm_prefork:notice] [pid 862] AH00163: Apache/2.4.25 (Debian) OpenSSL/1.0.2l configured -- resuming normal operations",
                    "variable": {
                        name: "L",
                        value: "4320"
                    },
                    "date": new Date("2018-04-01T15:59:20.914Z"),
                },
                {
                    "lineNumber": 14,
                    "_id": "5afaf778e8de5339aa18f52f",
                    "filename": "error.log.12.gz",
                    "path": "~/logs",
                    "line": "[Sun Apr 01 17:36:42.474367 2018] [mpm_prefork:notice] [pid 878] AH00163: Apache/2.4.25 (Debian) OpenSSL/1.0.2l configured -- resuming normal operations",
                    "variable": {
                        name: "L",
                        value: "432"
                    },
                    "date": new Date("2018-04-01T15:36:42.474Z"),
                },
                {
                    "lineNumber": 18,
                    "_id": "5afaf778e8de5339aa18f530",
                    "filename": "error.log.12.gz",
                    "path": "~/logs",
                    "line": "[Sun Apr 01 17:59:20.914307 2018] [mpm_prefork:notice] [pid 862] AH00163: Apache/2.4.25 (Debian) OpenSSL/1.0.2l configured -- resuming normal operations",
                    "variable": {
                        name: "L",
                        value: "432"
                    },
                    "date": new Date("2018-04-01T15:59:20.914Z"),
                },
                {
                    "lineNumber": 22,
                    "_id": "5afaf778e8de5339aa18f531",
                    "filename": "error.log.12.gz",
                    "path": "~/logs",
                    "line": "[Sun Apr 01 18:04:37.754805 2018] [mpm_prefork:notice] [pid 809] AH00163: Apache/2.4.25 (Debian) OpenSSL/1.0.2l configured -- resuming normal operations",
                    "variable": {
                        name: "L",
                        value: "432"
                    },
                    "date": new Date("2018-04-01T16:04:37.754Z"),
                }],
            _id: "5af76dd7fc763007009dda89",
            requiredVariablesSets: [
                [{
                    name: "K",
                    value: "12",
                }, {
                    name: "L",
                    value: "123",
                    
                }, {
                    name: "M",
                    value: "543",
                }],
                [{
                    name: "K",
                    value: "321",
                }, {
                    name: "L",
                    value: "432",
                    nextVariables: [
                        {
                            name: "K"
                        }
                    ]
                }, {
                    name: "M",
                    value: "543"
                }],
                [{
                    name: "K",
                    value: "120",
                }]
            ],
            hostId: {
                paths: [{
                    path: "~/logs",
                    pattern: "dds"
                }, {
                    path: "~/logs2",
                    pattern: "dsa"
                }],
                _id: "5af60328e8e2c32598a5dcd3",
                name: "sens2015",
                address: "s8.mydevil.net",
                protocolType: "ssh",
                login: "sens2015",
                password: "pE1YrOBGB%SEVm9TAZ!4",
                __v: 0
            },
            description: "Test powiadomienie 3",
            email: "michalkolbusz@gmail.com",
            schedule: "* * * * * *",
            __v: 0
        }

        filesFromDBMock = [
            {
                _id: "5afb7fcce9625742316d7a17",
                visitedLinesNumber: -1,
                filename: "access.log.2.gz",
                path: "~/logs2",
                hostId: "5af60328e8e2c32598a5dcd3",
                size: 75,
                last_modified: new Date("2018-04-11T14:34:16.000Z"),
                update_at: new Date("2018-05-16T00:48:12.188Z"),
                timePattern: {
                    pattern: "\d{2} .{3} \\d{2} \\d{2}:\\d{2}:\\d{2}.\\d{6} \\d{4}",
                    datetimeFormat: "ddd MMM DD HH:mm:ss YYYY"
                },
                content: "127.0.0.1 - - [11/Apr/2018:16:34:16 +0200] \"-\" 408 0 \"-\" \"-\"\n127.0.0.1 - - [11/Apr/2018:16:34:16 +0200] \"-\" 408 0 \"-\" \"-\"\n127.0.0.1 - - [11/Apr/2018:16:34:16 +0200] \"-\" 408 0 \"-\" \"-\"\n"
            }
        ]
        done();
    });

    it('should get logs with newer last modified date', (done) => {
        let getLogsToCheckByRules = notificationUtils.__get__("getLogsToCheckByRules");

        let logs = [
            {
                "last_modified": new Date("2018-04-24T10:29:00.000Z"),
                "_id": "5afaae0320aa510b7a088437"
            },
            {
                "last_modified": new Date("2018-05-16T23:24:18.000Z"),
                "_id": "5afaae0320aa510b7a088438"
            },
            {
                "last_modified": new Date("2018-04-12T22:09:32.000Z"),
                "_id": "5afaae0320aa510b7a088439"
            },
            {
                "last_modified": new Date("2018-04-12T16:03:42.000Z"),
                "_id": "5afaae0320aa510b7a08843a"
            },
            {
                "last_modified": new Date("2018-04-03T22:09:59.000Z"),
                "_id": "5afaae0320aa510b7a08843b"
            },
            {
                "last_modified": new Date("2018-04-02T22:06:16.000Z"),
                "_id": "5afaae0320aa510b7a08843c"
            },
            {
                "last_modified": new Date("2018-04-01T22:08:59.000Z"),
                "_id": "5afaae0320aa510b7a08843d"
            }
        ];

        let newLogs = getLogsToCheckByRules(logs, notificationMock.visitedLogs);

        newLogs.should.be.an('array').that.has.lengthOf(4).and.include(logs[logs.length -1]);
        done();
    })

    it('should get date from string with given format and pattern', (done) => {
        let lines = [{
            line: "[Sun Apr 01 17:59:20.914307 2018] [mpm_prefork:notice] [pid 862] AH00163: Apache/2.4.25 (Debian) OpenSSL/1.0.2l configured -- resuming normal operations",
            path: {
                pattern: ".{3} .{3} \\d{2} \\d{2}:\\d{2}:\\d{2}.\\d{6} \\d{4}",
                datetimeFormat: "ddd MMM DD HH:mm:ss YYYY"
            }
        },{
            line: '192.168.195.131 - - [13/Apr/2018:12:53:57 +0200] "GET / HTTP/1.1" 200 10956 "-" "avast! Antivirus',
            path: {
                pattern: "\\d{2}/.{3}/\\d{4}:\\d{2}:\\d{2}:\\d{2}\\s.{5}",
                datetimeFormat: "DD/MMM/YYYY:HH:mm:ss"
            }
        }]

        let checkDateOfDanger = notificationUtils.__get__("checkDateOfDanger");

        let result = checkDateOfDanger(lines[0].line, lines[0].path);
        result.should.be.a("string").that.is.equal("2018 04 01 17:59");

        result = checkDateOfDanger(lines[1].line, lines[1].path);
        result.should.be.a("string").that.is.equal("2018 04 13 12:53");
        
        done();
    });

    it('should get empty string if bad pattern of date', (done) => {
        let lines = [{
            line: "[Sun Apr 01 17:59:20.914307 2018] [mpm_prefork:notice] [pid 862] AH00163: Apache/2.4.25 (Debian) OpenSSL/1.0.2l configured -- resuming normal operations",
            path: {
                pattern: ".{2} .{3} \\d{2} \\d{2}:\\d{2}:\\d{2}.\\d{6} \\d{4}$",
                datetimeFormat: "ddd MMM DD HH:mm:ss YYYY"
            }
        },{
            line: '192.168.195.131 - - [13/Apr/2018:12:53:57 +0200] "GET / HTTP/1.1" 200 10956 "-" "avast! Antivirus',
            path: {
                pattern: "d\\d{2}/.{3}/\\d{4}:\\d{2}:\\d{2}:\\d{2}\\s.{5}",
                datetimeFormat: "DD/MMM/YYYY:HH:mm:ss"
            }
        }]

        let checkDateOfDanger = notificationUtils.__get__("checkDateOfDanger");

        let result = checkDateOfDanger(lines[0].line, lines[0].path);
        result.should.be.a("string").that.is.equal("");

        result = checkDateOfDanger(lines[1].line, lines[1].path);
        result.should.be.a("string").that.is.equal("");
        
        done();
    });

    it('should get empty string if bad format of date', (done) => {
        let lines = [{
            line: "[Sun Apr 01 17:59:20.914307 2018] [mpm_prefork:notice] [pid 862] AH00163: Apache/2.4.25 (Debian) OpenSSL/1.0.2l configured -- resuming normal operations",
            path: {
                pattern: ".{2} .{3} \\d{2} \\d{2}:\\d{2}:\\d{2}.\\d{6} \\d{4}",
                datetimeFormat: "ddddd MMM DD HH:mm:ss YYYY"
            }
        },{
            line: '192.168.195.131 - - [13/Apr/2018:12:53:57 +0200] "GET / HTTP/1.1" 200 10956 "-" "avast! Antivirus',
            path: {
                pattern: "\\d{2}/.{3}/\\d{4}:\\d{2}:\\d{2}:\\d{2}\\s.{5}",
                datetimeFormat: "DD/MM/YYYY:HH:mm:ss"
            }
        }]

        let checkDateOfDanger = notificationUtils.__get__("checkDateOfDanger");

        let result = checkDateOfDanger(lines[0].line, lines[0].path);
        result.should.be.a("string").that.is.equal("");

        result = checkDateOfDanger(lines[1].line, lines[1].path);
        result.should.be.a("string").that.is.equal("");
        
        done();
    });

    it('should check variables passing', (done) => {
    
        let checkVariables = notificationUtils.__get__("checkVariables");

        let result = checkVariables(notificationMock);
        result.should.be.a("array").that.has.lengthOf(3).and.is.deep.equal([false, true, false]);
        
        done();
    });

    it('should check the wrong order of variables', (done) => {
        let checkVariables = notificationUtils.__get__("checkVariables");
        notificationMock.files[0].date = new Date("2015-04-06T23:48:33.932Z");
        let result = checkVariables(notificationMock);
        result.should.be.a("array").that.has.lengthOf(3).and.is.deep.equal([false, false, false]);
        
        done();
    });

    it('should find date from file with first occurence of variable', (done) => {
        let findFirstOccurence = notificationUtils.__get__("findFirstOccurence");

        let result = findFirstOccurence(notificationMock.requiredVariablesSets[1][0], notificationMock.files);
        result.should.be.an.instanceOf(Date).and.be.deep.equal(new Date("2018-04-06T23:48:33.932Z"));
        done();
    })

    it('should find date from file with last occurence of variable', (done) => {
        let findLastOccurence = notificationUtils.__get__("findLastOccurence");

        let result = findLastOccurence(notificationMock.requiredVariablesSets[1][1], notificationMock.files);
        result.should.be.an.instanceOf(Date).and.be.deep.equal(new Date("2018-04-01T16:04:37.754Z"));
        done();
    })

    it('should create clean variables sets for notification from rulessets', (done) => {
        let createNotificationVariablesFrom = notificationUtils.__get__("createNotificationVariablesFrom");
        let result = createNotificationVariablesFrom(notificationMock.requiredVariablesSets);

        result.should.be.an("array").that.is.deep.equal(
            [{
                K: [],
                L: [],
                M: []
            }, {
                K: [],
                L: [],
                M: []
            }, {
                K: []
            }]
        )
        done()
    })

    it('should create Date object from the other with the same hour and minutes', (done) => {
        let getDataFromTime = notificationUtils.__get__("getDataFromTime");
        let result = getDataFromTime(new Date("2018-04-01T15:59:20.914Z"));

        result.should.be.instanceOf(Date);
        result.getHours().should.be.a("number").and.be.equal(17);
        result.getMinutes().should.be.a("number").and.be.equal(59);
        done();
    })

    it('should clean the right set of variables if values have been set properly', (done) => {
        let cleanVariablesFrom = notificationUtils.__get__("cleanVariablesFrom");
        cleanVariablesFrom(notificationMock, [false, true, false]);
        
        notificationMock.should.be.an("object").and.have.property("variables")
        notificationMock.variables.should.be.an("array").and.eql(
            [{
                K: ["12"],
                L: [],
                M: ["543"]
            }, {
                K: [],
                L: [],
                M: []
            }, {
                K: ["12"],
            }]
        )
        done();
    })

    it('should check if date is between range', (done) => {
        let isGoodTime = notificationUtils.__get__("isGoodTime");

        let ruleTime = {
            startRuleTime: new Date("2000-01-12 15:34"),
            stopRuleTime: new Date("2000-01-12 15:37")
        }
        let result = isGoodTime(ruleTime, new Date("2000-01-12 15:35"));
        result.should.be.an("boolean").and.be.true;

        ruleTime = {
            startRuleTime: new Date("2000-01-12 15:34"),
            stopRuleTime: new Date("2000-01-14 15:37")
        }
        result = isGoodTime(ruleTime, new Date("2000-01-13 09:00"));
        result.should.be.an("boolean").and.be.true;
        done();
    })

    it('should check if date is not between range', (done) => {
        let isGoodTime = notificationUtils.__get__("isGoodTime");

        let result = isGoodTime(new Date("2000-01-12 15:34"), new Date("2000-01-12 15:37"), new Date("2000-01-12 15:38"));
        result.should.be.an("boolean").and.be.false;
        
        result = isGoodTime(new Date("2000-01-12 15:34"), new Date("2000-01-13 15:37"), new Date("2000-01-15 15:35"));
        result.should.be.an("boolean").and.be.false;
        done();
    })

    it('should check if variable is in required variables', (done) => {
        let isInReqiuredVariables = notificationUtils.__get__("isInReqiuredVariables");
        
        let variable = {name: 'K', value: 100};
        let result = isInReqiuredVariables(variable, notificationMock.requiredVariablesSets);
        result.should.be.true;
        done();
    })

    it('should check if variable is not in required variables', (done) => {
        let isInReqiuredVariables = notificationUtils.__get__("isInReqiuredVariables");
        
        let variable = {name: 'P', value: 100};
        let result = isInReqiuredVariables(variable, notificationMock.requiredVariablesSets);
        result.should.be.false;
        done();
    })

    // it('should set notification\'s variales from rules sets', (done) => {
    //     let performAllRulesSetsFor = notificationUtils.__get__("performAllRulesSetsFor");
        
    //     Log.find({ hostId: hostId }, { last_modified: 1 }, (err, logs) => {
    //         if (err) throw err;

    //         var logsToCheck = getLogsToCheckByRules(logs, notification.visitedLogs);

    //         Log.find({ _id: { $in: logsToCheck } }, (err, filesFromDB) => {
    //             performAllRulesSetsFor(notificationMock, filesFromDB);

    //         }
    //     }
    //     done();
    // })

    it('should check if mail was sent', (done) => {
        let sendMailByNodemailer = notificationUtils.__get__("sendMailByNodemailer");
    
        nodemailer.createTestAccount((err, account) => {
            let settings = {
                data: {
                    serverSmtp: 'smtp.ethereal.email',
                    port: 587,
                    secure: false, // true for 465, false for other ports
                    username: account.user, // generated ethereal user
                    password: account.pass  // generated ethereal password
                }
            }

            let shouldNotify = [false, true, false];
                
            let result = sendMailByNodemailer(notificationMock, settings, shouldNotify);

            result.should.be.a("promise");
            result.then(info => {
                done();
            })

        });
        
    })

    it('should check if mail was not sent', (done) => {
        let sendMailByNodemailer = notificationUtils.__get__("sendMailByNodemailer");
    
        nodemailer.createTestAccount((err, account) => {
            let settings = {
                data: {
                    serverSmtp: 'smt.ethereal.email',
                    port: 123,
                    secure: false, // true for 465, false for other ports
                    username: account.user, // generated ethereal user
                    password: account.pass  // generated ethereal password
                }
            }

            let shouldNotify = [false, true, false];
                
            let result = sendMailByNodemailer(notificationMock, settings, shouldNotify);

            result.should.be.a("promise");
            result.then(info => {
            }, err => {
                done();
            })

        });
    })
    

    after((done) => {
        mongoose.connection.close()
        done();
    })

});