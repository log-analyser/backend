module.exports = {
    filesDownloadedSavingPath: './logs',
    databaseURL: 'mongodb://backend:backend@ds016138.mlab.com:16138/loganalyser2',
    frontendURL: 'http://localhost:4200',
    port: 5000,
    ports: {
        ftp: 21,
        ssh: 22,
        http: 80,
        https: 443
    }
}