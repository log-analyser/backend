var Host = require('../models/host.model');
var Notification = require('../models/notification.model');

module.exports = {
    postHost,
    getHosts,
    getHost,
    deleteHost,
    putHost,
    deleteFiles
};

/**
 * This function downloads one host from database and return it to client
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function getHost(req, res) {
    Host.find({ _id: req.params.id }, (err, host) => {
        if (err) console.log(err);
        res.send(host[0]);
    });
}

/**
 * This function adds one host to database
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function postHost(req, res) {
    console.log("post host");

    var host = new Host(req.body);
    console.log(req.body);
    host.protocolType = host.protocolType.value;

    host.save((err, host) => {
        if (err) throw err;
        res.send(host);
    });
}

/**
 * This function downloads all hosts from database and return their to client
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function getHosts(req, res) {
    console.log("getAllHosts");
    Host.find({}, (err, hosts) => {
        if (err) {
            res.json(err);
            console.log(err);
        }

        res.json(hosts);
    });
}

/**
 * This function removes one host from database
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function deleteHost(req, res) {
    console.log("delete host");
    var id = req.params.id;
    Host.findById(id, function(err, host) {
        if (err) throw err;
        if (host) {
            var hostId = host._id;
            Notification.find({ hostId: hostId }, (err, notifications) => {
                if (err) {
                    console.log(err);
                }

                notifications.forEach(notification => {
                    notification.remove();
                });
            });
            host.remove();
            console.log("deleted host");
            res.send(host);
        } else {
            console.log("no host");
            res.send({});
        }
    });
}

/**
 * This function removes files from host from database
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function deleteFiles(req, res) {
    console.log("delete files from host");
    var id = req.params.id;

    Host.findById(id, function(err, host) {
        if (err) throw err;
        if (host) {
            host.rulesSet.forEach(rulesSet => {
                rulesSet.variables.forEach(variable => {
                    variable.files = [];
                    variable.value = '';
                });
            });

            Host.findByIdAndUpdate(id, { $set: host }, { new: true },
                function(err, updatedHost) {
                    if (err) throw err;
                    console.log('Edited host with id=' + id);
                    console.log(updatedHost);
                    res.send(updatedHost);
                }
            );
        } else {
            console.log("No host with id = " + id + ")");
            res.send("ERROR: No host with id = " + id + ")");
        }
    });
}

/**
 * This function edits one host in database
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function putHost(req, res) {
    console.log("edit host");
    var id = req.params.id;

    var host = new Host(req.body);
    host.protocolType = host.protocolType.value;
    Host.findByIdAndUpdate(id, { $set: host }, { new: true },
        function(err, updatedHost) {
            if (err) throw err;
            console.log('Edited host with id=' + id);
            res.send(updatedHost);
        }
    );
}