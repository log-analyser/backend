const schedule = require('node-schedule');
var Notification = require('../models/notification.model');
var notificationUtils = require('../utils/notifications.utils');

var scheduledNotifications = {};

module.exports = {
    postNotification,
    getNotifications,
    getNotification,
    deleteNotification,
    putNotification,
    runNotification,
    scheduleNotification,
    stopScheduledNotification
};

/**
 * This function adds one notification to database
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function postNotification(req, res) {
    var notificationRawData = req.body;
    var notification = new Notification(notificationRawData);
    notification.files = [];
    notification.visitedLogs = [];
    notification.variables = notificationUtils.createNotificationVariablesFrom(notification.requiredVariablesSets);

    notification.save((err, notification) => {
        if (err) throw err;
        res.send(notification);
    });
}

/**
 * This function downloads all notifications from database and return their to client
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function getNotifications(req, res) {
    Notification.find().populate('hostId').populate('rulesSets').exec((err, result) => {

        if (err) {
            console.error(err);
            res.json(err);
            return;
        }
        res.json(result);
    });
}

/**
 * This function downloads one notification from database and return it to client
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function getNotification(req, res) {
    Notification.findOne({ _id: req.params.id }).populate('hostId').populate('rulesSets').exec((err, result) => {
        if (err) {
            console.error(err);
            return null;
        }
        res.json(result);
    });
}

/**
 * This function removes one notification from database
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function deleteNotification(req, res) {
    console.log("delete notification");
    var id = req.params.id;
    Notification.findById(id, function(err, notification) {
        if (err) throw err;
        if (notification) {
            notification.remove();
            res.send(notification);
        } else {
            res.send("ERROR: No notification with id = " + id);
        }
    });
}

/**
 * This function edits one notification in database
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function putNotification(req, res) {
    console.log("put notification");
    var notificationRawData = req.body;
    var notification = new Notification(notificationRawData);

    notification.variables = notificationUtils.createNotificationVariablesFrom(notification.requiredVariablesSets);

    Notification.findByIdAndUpdate(notification._id, { $set: notification }, { new: true },
        function(err, updatedNotification) {
            if (err) throw err;
            res.send(updatedNotification);
        }
    );
}

/**
 * This function runs notification periodically
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function scheduleNotification(req, res) {
    Notification.findOne({ _id: req.params.id }).populate('hostId').populate('rulesSets').exec((err, notification) => {
        if (err) throw err;
        if (notification) {
            console.log('notification.schedule', notification.schedule);
            var j = schedule.scheduleJob(notification.schedule, function() {
                notificationUtils.run(notification);
            });

            if (j) {
                notification.inProgress = true;
                scheduledNotifications[notification._id] = j;
                res.json(notification);
            } else {
                res.status(500).send();
            }

        }
    });
}

/**
 * This function stops notification if is on
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function stopScheduledNotification(req, res) {
    let notificationSchedule = scheduledNotifications[req.params.id];
    if (notificationSchedule) {
        notificationSchedule.cancel();
        delete scheduledNotifications[req.params.id];
    }
    Notification.findByIdAndUpdate(req.params.id, { $set: { inProgress: false } }, { new: true }).populate('hostId').populate('rulesSets').exec(
        function(err, updatedNotification) {
            if (err) throw err;
            res.send(updatedNotification);
        }
    );
}

/**
 * This function runs notification once
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function runNotification(req, res) {
    Notification.findOne({
        _id: req.params.id
    }).populate('hostId').populate('rulesSets').exec((err, notification) => {
        if (err) throw err;
        if (notification) {
            notificationUtils.run(notification);

            res.send(notification);
        }
    });
}