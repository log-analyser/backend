var Setting = require('../models/setting.model');

module.exports = {
    postSetting,
    getSettings,
    getSetting,
    deleteSetting,
    putSetting
};

/**
 * This function downloads one settings object from database and return it to client
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function getSetting(req, res) {
    Setting.find({ _id: req.params.id }, (err, setting) => {
        if (err) throw err;
        res.send(setting[0]);
    });
}

/**
 * This function adds one settings object to database
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function postSetting(req, res) {
    console.log("post setting");
    var setting = new Setting(req.body);

    setting.save((err, setting) => {
        if (err) throw err;
        res.send(setting);
    });
}

/**
 * This function downloads all settings from database and return their to client
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function getSettings(req, res) {
    Setting.find({}).populate('hostId', '-paths').exec((err, result) => {
        if (err) {
            console.error(err);
            res.json(err);
            return;
        }

        res.json(result);
    });
}

/**
 * This function removes one settings object from database
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function deleteSetting(req, res) {
    console.log("delete setting");
    var id = req.params.id;
    Setting.findById(id, function(err, setting) {
        if (err) throw err;
        if (setting) {
            setting.remove();
            console.log("Deleted setting with id = " + id + ")");
            res.send(setting);
        } else {
            console.log("No setting with id = " + id + ")");
            res.send("ERROR: No setting with id = " + id + ")");
        }
    });
}

/**
 * This function edits one settings object in database
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function putSetting(req, res) {
    console.log("edit setting");
    var setting = new Setting(req.body);

    Setting.findOneAndUpdate({ name: setting.name }, { $set: setting }, { new: true },
        function(err, updatedSetting) {
            if (err) throw err;
            console.log('Edited setting with id=' + setting._id);
            console.log(updatedSetting);
            res.send(updatedSetting);
        }
    );
}