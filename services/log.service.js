var Log = require('../models/log.model');
var logsUtils = require('../utils/logs.utils');

module.exports = {
    getLogsCount,
    getLogsBasicData,
    getLogContent,
    deleteLog,
    deleteLogs,
    downloadLogs
};

/**
 * This function counts all logs objects in database
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function getLogsCount(req, res) {
    Log.count({}, (err, count) => {
        if (err) {
            res.json(err);
            console.error(err);
        }
        res.json(count);
    });
}

/**
 * This function downloads all logs from database and return their to client.
 * Content is not sent do the client.
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function getLogsBasicData(req, res) {
    let limit = parseInt(req.query.limit);
    let skip = parseInt(req.query.skip);
    let query = Log.find().select("-content").limit(limit).skip(skip);
    query.exec((err, logs) => {
        if (err) {
            res.json(err);
            console.error(err);
        }
        res.json(logs);
    });
}

/**
 * This function downloads content of log from database and return their to client.
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function getLogContent(req, res) {
    console.log("getLogContent");
    var id = req.params.id;
    let query = Log.findOne({ _id: id }).select("content");
    query.exec((err, content) => {
        if (err) {
            res.json(err);
            console.log(err);
        }
        res.json(content);
    });
}

/**
 * This function removes all logs from database
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function deleteLogs(req, res) {
    console.log("delete all Logs");
    Log.find({}, (err, logs) => {
        if (err) {
            res.json(err);
            console.err(err);
        }
        logs.forEach(log => {
            log.remove();
        });
        res.send(logs);
    });
}

/**
 * This function removes one log from database
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function deleteLog(req, res) {
    console.log("delete log");
    var id = req.params.id;
    Log.findById(id, function(err, log) {
        if (err) throw err;
        if (log) {
            log.remove();
            console.log("Deleted log (id = " + id + ")");
            res.send(log);
        } else {
            console.log("No log with id = " + id + ")");
            res.send("ERROR: No log with id = " + id + ")");
        }
    });
}

/**
 * This function downloads logs from server
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function downloadLogs(req, res) {
    logsUtils.downloadLogsForNotification(req.params.hostId).then(
        files => res.json(files),
        err => res.json(err)
    );
}