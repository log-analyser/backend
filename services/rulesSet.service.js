var Host = require('../models/host.model');
var RulesSet = require('../models/rulesSet.model');


module.exports = {
    postRulesSet,
    getRulesSets,
    getRulesSet,
    deleteRulesSet,
    putRulesSet,
    getRulesSetsByHostId
};

/**
 * This function downloads all rulesSets belongs to specified host 
 * from database and return their to client
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function getRulesSetsByHostId(req, res) {
    RulesSet.find({ hostId: req.params.hostId }, (err, rulesSets) => {
        if (err) throw err;
        res.send(rulesSets);
    });
}

/**
 * This function downloads all rulesSets from database and return their to client
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function getRulesSets(req, res) {
    RulesSet.find({ hostId: req.params.hostId }, (err, rulesSets) => {
        if (err) throw err;
        res.send(rulesSets);
    });
}

/**
 * This function downloads one rulesSet from database and return it to client
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function getRulesSet(req, res) {
    RulesSet.findOne({ _id: req.params.id }, (err, rulesSet) => {
        if (err) throw err;
        res.send(rulesSet);
    });
}

/**
 * This function adds one rulesSet to database
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function postRulesSet(req, res) {
    console.log("post rulesSet");
    var rulesSet = new RulesSet(req.body);

    rulesSet.save((err, rulesSet) => {
        if (err) throw err;
        res.send(rulesSet);
    });

}

/**
 * This function edits host while rulesSet is removing
 * @param {Object} host host to modify
 */
function updateHostsRulesSet(host) {
    return new Promise((resolve, reject) => {
        console.log("update HOST:");
        console.log(host.rulesSet[0].variables);
        Host.findOneAndUpdate({ _id: host._id }, {
                $set: {
                    rulesSet: host.rulesSet,
                }
            }, { new: true },
            (err, newHost) => {
                if (err) reject(err);
                console.log('Edited host with id=' + host._id);
                console.log(host.rulesSet);
                resolve(newHost);
            }
        );
    });
}

/**
 * This function removes one rulesSet from database
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function deleteRulesSet(req, res) {
    var id = req.params.id;
    console.log("delete rulesSet");
    RulesSet.findById(id, function(err, rulesSet) {
        if (err) throw err;

        var rulesSetId = rulesSet._id;
        Host.find({ '_id': { $in: rulesSet.hostIds } }, (err, hosts) => {
            hosts.forEach(host => {
                host.rulesSet = host.rulesSet.filter(ruleSet => {
                    ruleSet.rulesSetId != rulesSetId;
                });
                updateHostsRulesSet(host);
            });
        });
        rulesSet.remove();
        res.send(rulesSet);
    });
}

/**
 * This function edits one rulesSet in database
 * @param {Object} req request from client 
 * @param {Object} res response to client
 */
function putRulesSet(req, res) {
    console.log("edit rulesSet");
    var id = req.params.id;

    RulesSet.findOneAndUpdate({ _id: id }, {
            $set: {
                hostIds: req.body.hostIds,
                variables: req.body.variables
            }
        }, {
            new: true
        },
        function(err, newRulesSet) {
            if (err) throw err;
            console.log('Edited rulesSet with id=' + id);
            res.send(newRulesSet);
        }
    );
}