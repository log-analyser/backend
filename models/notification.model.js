var mongoose = require('mongoose');
var DetectedLogEntrySchema = require('./detectedLogEntry.model');
var deepPopulate = require('mongoose-deep-populate')(mongoose);

var Schema = mongoose.Schema;
var NotificationSchema = new Schema({
    hostId: { type: Schema.Types.ObjectId, ref: 'Host' },
    description: { type: String, required: true },
    email: { type: String, required: false },
    inProgress: { type: Boolean, required: false },
    rulesSets: [{ type: Schema.Types.ObjectId, ref: 'RulesSet', required: true }],
    date: { type: Date, required: false },
    schedule: { type: String, required: false },
    files: { type: [DetectedLogEntrySchema], required: false, default: [] },
    visitedLogs: { type: [Schema.Types.Mixed], required: false, default: [] },
    variables: { type: [Schema.Types.Mixed] },
    requiredVariablesSets: [
        [Schema.Types.Mixed]
    ]
});
NotificationSchema.plugin(deepPopulate);

var Notification = mongoose.model('Notification', NotificationSchema);
module.exports = Notification;