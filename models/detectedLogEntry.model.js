var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var DetectedLogEntrySchema = new Schema({
    filename: { type: String, required: true },
    variablename: { type: String, required: true },
    lineNumber: { type: Number, required: true, default: 0 },
    line: { type: String, required: false },
    path: { type: String, required: false },
    date: { type: Date, required: false },
    variable: { type: Schema.Types.Mixed, required: false },
    toSend: { type: Number, required: false, default: 0 }
});
var DetectedLogEntry = mongoose.model('DetectedLogEntry', DetectedLogEntrySchema);
module.exports = DetectedLogEntrySchema;