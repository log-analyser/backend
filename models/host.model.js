var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var HostSchema = new Schema({
    name: { type: String, required: true },
    address: { type: String, required: true },
    port: { type: Number, required: false },
    protocolType: { type: Schema.Types.Mixed, required: true },
    paths: { type: [], required: true },
    login: { type: String, required: false },
    password: { type: String, required: false }
});

var Host = mongoose.model('Host', HostSchema);
module.exports = Host;