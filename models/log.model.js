var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var LogSchema = new Schema({
    filename: { type: String, required: true },
    path: { type: String },
    hostId: { type: String, required: true },
    timePattern: { type: Schema.Types.Mixed, required: false },
    content: { type: String },
    size: { type: Number },
    last_modified: { type: Date },
    update_at: { type: Date, default: Date.now, required: true },
    visitedLinesNumber: { type: Number, default: -1 }
});

var Log = mongoose.model('Log', LogSchema);
module.exports = Log;