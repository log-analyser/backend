var mongoose = require('mongoose');
var LogSchema = require('./log.model')

var Schema = mongoose.Schema;
var visitedLogSchema = new Schema({
    logId: { type: Schema.Types.ObjectId, ref: 'Log', required: true },
    lineNumber: { type: Number, default: -1 },
    date: { type: Date, required: true }
});

var visitedLog = mongoose.model('visitedLog', visitedLogSchema);
module.exports = visitedLogSchema;