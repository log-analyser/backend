var mongoose = require('mongoose');
var RuleSchema = require('./rule.model');

var Schema = mongoose.Schema;
var VariableSchema = new Schema({
    name: { type: String, required: true },
    rules: { type: [RuleSchema] },
});

module.exports = VariableSchema;