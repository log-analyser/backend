var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var SettingSchema = new Schema({
    name: { type: String, required: true },
    data: { type: Schema.Types.Mixed }
});

var Setting = mongoose.model('Setting', SettingSchema);
module.exports = Setting;