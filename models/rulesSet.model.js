var mongoose = require('mongoose');
var VariableSchema = require('./variable.model');

var Schema = mongoose.Schema;
var RulesSetSchema = new Schema({
    name: { type: String, required: true },
    variables: { type: [VariableSchema], required: true }
});

var RulesSet = mongoose.model('RulesSet', RulesSetSchema);
module.exports = RulesSet;