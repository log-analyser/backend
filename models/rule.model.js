var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var RuleSchema = new Schema({
    regExp: { type: String, required: true },
    startTime: { type: Date, required: true },
    stopTime: { type: Date, required: true },
    value: { type: String, required: true },
});

module.exports = RuleSchema;