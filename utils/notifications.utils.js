const moment = require('moment');
var Notification = require('../models/notification.model');
var Log = require('../models/log.model');
var logsUtils = require('../utils/logs.utils');
var Setting = require('../models/setting.model');
const nodemailer = require('nodemailer');
const schedule = require('node-schedule');
const hbs = require('nodemailer-express-handlebars');

module.exports = {
    createNotificationVariablesFrom,
    getDataFromTime,
    getLogsToCheckByRules,
    run,
    cleanVariablesFrom
};

/**
 * This function creates variables in notification object for each set of required variables
 * @param {[]} requiredVariablesSets all sets of variables which are explored in notification
 */
function createNotificationVariablesFrom(requiredVariablesSets) {
    var variables = [];

    requiredVariablesSets.forEach(reqVar => {
        var notificationVariables = {};
        reqVar.forEach(variable => {
            notificationVariables[variable.name] = [];
        });
        variables.push(notificationVariables);
    });
    return variables;
}

/**
 * This function sets date for time from rule
 * @param {Date} date date from rule
 */
function getDataFromTime(date) {
    date.setFullYear(2000, 0, 1);
    return new Date(date.setHours(date.getHours(), date.getMinutes()));
}

/**
 * This function finds log files which should be penetrate
 * @param {[Log]} logs logs to filter
 * @param {[]} visitedLogs info about logs which was penetrated
 */
function getLogsToCheckByRules(logs, visitedLogs) {
    return logs.filter(date => {
        var visitedLog = visitedLogs.find(visitedLog => visitedLog.logId == date._id);
        if (visitedLog) {
            return visitedLog.date < date.last_modified;
        } else {
            return true;
        }
    });
}

/**
 * This function checks whether the variable is in set of required variables
 * @param {Variable} variable the variable being tested
 * @param {[]} requiredVariablesSets set of required variables
 */
function isInReqiuredVariables(variable, requiredVariablesSets) {

    var isInArray = false;

    requiredVariablesSets.forEach(requiredVariables => {
        requiredVariables.forEach(reqVariable => {
            if (reqVariable.name == variable.name) {
                isInArray = true;
            }
        });
    });
    return isInArray;
}

/**
 * This  function runs process of notification: downloads logs, check all rulesSets,
 * sets notification 's variables, notify about danger
 * @param {Notification} notification notification to start
 */
function run(notification) {
    console.log("start notification");
    var hostId = notification.hostId._id;

    // download logs
    logsUtils.downloadLogsForNotification(hostId).then(
        () => {
            console.log("logs downloaded");
            Log.find({ hostId: hostId }, { last_modified: 1 }, (err, logs) => {
                if (err) throw err;

                var logsToCheck = getLogsToCheckByRules(logs, notification.visitedLogs);

                Log.find({ _id: { $in: logsToCheck } }, (err, filesFromDB) => {

                    // check all rulesSets, variables, rulses and set notification's variables
                    performAllRulesSetsFor(notification, filesFromDB);

                    // add files to visitedLogs array or edit existing
                    addFilesToVisitedLogs(notification, filesFromDB);

                    // send mail if required
                    notifyAboutDanger(notification).then(_ => {
                            // save norification
                            saveNotification(notification);
                        },
                        err => { console.log(err); }
                    );

                });
            });
        },
        _ => {}
    );
}

// Removes variables which occures in danger situaction that generated notification
function cleanVariablesFrom(notification, shouldNotify) {
    shouldNotify.forEach((shouldClean, index) => {
        if (shouldClean) {
            Object.keys(notification.variables[index]).forEach(function(key) {
                notification.variables[index][key] = [];
            });
        }
    });
}

// Finds date of the first occurence of danger
function findFirstOccurence(variable, files) {
    var first = new Date();
    files.forEach(file => {
        if (file.variable.name == variable.name && file.variable.value == variable.value && file.date < first) {
            first = file.date;
        }
    });
    return first;
}

// Finds date of the last occurence of danger
function findLastOccurence(variable, files) {
    var last = new Date(1970, 1, 1);
    files.forEach(file => {
        if (file.variable.name == variable.name && file.variable.value == variable.value && file.date > last) {
            last = file.date;
        }
    });
    return last;
}

// Finds date of the first occurence of danger
function hasNextVariable(requiredVariable) {
    return requiredVariable.nextVariables && requiredVariable.nextVariables.length;
}

// Checks whether variables includes required variable
function isVariableInArray(requiredVariable, variables) {
    var isInTable = false;
    if (isValueInArray(requiredVariable.value, variables[requiredVariable.name])) {
        isInTable = true;
    }
    return isInTable;
}

// Checks whether notify about danger should be send
function checkVariables(notification) {
    var notifies = [];
    notification.requiredVariablesSets.forEach((requiredVariables, indexOfReqVars) => {
        var correctVariablesInSet = true;
        requiredVariables.forEach(requiredVariable => {
            var variables = notification.variables[indexOfReqVars];
            if (!isVariableInArray(requiredVariable, variables)) {
                correctVariablesInSet = false;
            }

            if (correctVariablesInSet && hasNextVariable(requiredVariable)) {
                requiredVariable['value'] = requiredVariables.find(rv => rv.name == requiredVariable.name).value;
                var firstOccuranceOfRequiredVariable = findFirstOccurence(requiredVariable, notification.files);

                requiredVariable.nextVariables.forEach(nextVariable => {
                    nextVariable['value'] = requiredVariables.find(rv => rv.name == nextVariable.name).value;
                    var timeOfNextVariableOccurence = findLastOccurence(nextVariable, notification.files);
                    if (firstOccuranceOfRequiredVariable > timeOfNextVariableOccurence) {
                        correctVariablesInSet = false;
                    }
                });
            }
        });
        notifies.push(correctVariablesInSet);
    });
    return notifies;
}

// Checks whether time of danger is within rules time frame
function isGoodTime(ruleTime, timeOfDanger) {
    return ruleTime.startRuleTime <= timeOfDanger && timeOfDanger <= ruleTime.stopRuleTime;
}

// Checks whether line of log contains the date in a proper format
function checkDateOfDanger(line, pattern) {
    var dateString = line.match(new RegExp(pattern.pattern));
    if (dateString) {
        var result = moment(dateString[0], pattern.datetimeFormat);
        return result.isValid() ? result.format("YYYY MM DD HH:mm") : "";
    } else {
        return "";
    }
}

// Saves visited logs in nottification
function addFilesToVisitedLogs(notification, filesFromDB) {
    var visLogs = {};
    filesFromDB.forEach((fileToVisit) => {
        var lineNumber = fileToVisit.content.split("\n").length;
        visLogs[fileToVisit._id] = {};
        visLogs[fileToVisit._id].lineNumber = lineNumber;
        visLogs[fileToVisit._id].date = fileToVisit.last_modified;
    });

    Object.keys(visLogs).forEach(logId => {
        visLogs[logId].logId = logId;
        var log = notification.visitedLogs.find((log) => {
            return log.logId == logId;
        });
        if (log) {
            Object.assign(log, visLogs[logId]);
        } else {
            console.log("push to visitedLogs");
            notification.visitedLogs.push(visLogs[logId]);
        }
    });
}

// Notifies about recognized danger
function notifyAboutDanger(notification) {
    return new Promise((resolve, reject) => {

        var shouldNotify = checkVariables(notification);
        console.log(shouldNotify);
        if (shouldNotify.indexOf(true) != -1) {

            cleanVariablesFrom(notification, shouldNotify);

            // MAIL
            if (notification.email) {
                console.log("send mail");
                sendMail(notification, shouldNotify).then(info => {
                        resolve(info);
                    },
                    err => reject(err));
            }
        } else {
            resolve(shouldNotify);
        }
    });
}

// Updates notification in database
function saveNotification(notification) {
    Notification.findByIdAndUpdate(notification._id, {
            $set: notification
        }, {
            new: true
        },
        function(err, updatedNotification) {
            if (err) throw err;
        }
    );
}

// Downloads start and stop time from rule
function getStartAndStopRuleTime(rule) {
    var startRuleDate = getDataFromTime(rule.startTime);
    var stopRuleDate = getDataFromTime(rule.stopTime);
    if (startRuleDate > stopRuleDate) {
        startRuleDate = new Date(startRuleDate.setDate(startRuleDate.getDate() - 1));
    }
    return {
        startRuleTime: startRuleDate.getTime(),
        stopRuleTime: stopRuleDate.getTime()
    };
}

// Checks time of danger based on date
function getTimeOfDanger(dateOfDangerString) {
    var realDateOfDanger = new Date(dateOfDangerString);
    var dateOfDanger = new Date(dateOfDangerString);
    dateOfDanger = new Date(dateOfDanger.setFullYear(2000, 0, 1));
    var timeOfDanger = dateOfDanger.getTime();
    return {
        time: timeOfDanger,
        realDate: realDateOfDanger
    };
}

// Returns object with information about how many lines was penetrated previously
function getNotificationFile(notification, file) {
    if (notification.visitedLogs.length == 0) {
        return { lineNumber: 0 };
    } else {
        var logID = notification.visitedLogs.find(log => log.logId == file._id);
        return logID ? logID : { lineNumber: 0 };
    }
}

// Checks whether the line includes the regexp 
function isLineDanger(line, regExp) {
    return regExp.test(line);
}

// Added information about danger to notification
function addLogEntryFileTo(notification, file, lineNumber, rule, variable, line, dangerOccurrence) {
    var logEntryFile = {
        filename: file.filename,
        path: file.path,
        lineNumber: lineNumber,
        line: line,
        variable: {
            value: rule.value,
            name: variable.name
        },
        date: dangerOccurrence.realDate,
        created_at: new Date(),
        toSend: 0
    };
    notification.files.push(logEntryFile);
}

// Create variables in notification
function saveVariablesIn(notification, variable, rule) {
    console.log("save: " + variable.name + " = " + rule.value);
    notification.variables.forEach(variablesObj => {
        if (isValueInArray(variable.name, Object.keys(variablesObj)) && !isValueInArray(rule.value, variablesObj[variable.name])) {
            variablesObj[variable.name].push(rule.value);
        }
    });
}

// Checks whether value exists in array
function isValueInArray(value, array) {
    return (array.indexOf(value) != -1) || (array.length == 0 && value == "");
}

// Test one rule on logs
function performRule(rule, variable, rulesSet, filesFromDB, notification) {
    var regExp = new RegExp(rule.regExp);
    var startAndStopRuleTime = getStartAndStopRuleTime(rule);

    // wybierz pliki, które spełniają regex (dla oszczędnośći czasu)
    var filesToVisit = filesFromDB.filter(file => regExp.test(file.content));

    filesToVisit.forEach(file => {
        var arrayOfLines = file.content.split("\n");
        let notificationFile = getNotificationFile(notification, file);

        // dla każdej linii w pliku
        arrayOfLines.filter((line, index) => index >= notificationFile.lineNumber - 1).forEach((line, index) => {

            var dateOfDangerString = checkDateOfDanger(line, file.timePattern);
            if (dateOfDangerString) {
                var dangerOccurrence = getTimeOfDanger(dateOfDangerString);
                if (isLineDanger(line, regExp) && isGoodTime(startAndStopRuleTime, dangerOccurrence.time)) {

                    addLogEntryFileTo(notification, file, notificationFile.lineNumber + index + 1, rule, variable, line, dangerOccurrence);
                    saveVariablesIn(notification, variable, rule);
                }
            }
        });
    });
}

// Test all rules on logs for one notification
function performAllRulesSetsFor(notification, filesFromDB) {
    // weż wszystkie rulesSety
    notification.rulesSets.forEach(rulesSet => {
        // weź wszystkie zmienne
        rulesSet.variables.filter(variable => isInReqiuredVariables(variable, notification.requiredVariablesSets)).forEach(variable => {
            // weź wszytkie reguły dla każdej zmiennej
            variable.rules.forEach(rule => {
                performRule(rule, variable, rulesSet, filesFromDB, notification);
            });
        });
    });
}

// Prepares data and sends mail with info about danger
function sendMail(notification, shouldNotify) {
    return new Promise((resolve, reject) => {
        Setting.findOne({ name: 'email' }, (err, settings) => {
            sendMailByNodemailer(notification, settings, shouldNotify).then(info => {
                    resolve(info);
                },
                _ => {}
            );
        });
    });
}

// Sends mail with info about danger
function sendMailByNodemailer(notification, settings, shouldNotify) {
    let transporter = nodemailer.createTransport({
        host: settings.data.serverSmtp,
        port: settings.data.port,
        secure: false, // true for 465, false for other ports
        auth: {
            user: settings.data.username,
            pass: settings.data.password
        }
    });

    transporter.use('compile', hbs({
        viewPath: 'views/email/',
        extName: '.hbs',
        viewEngine: '.hbs'
    }));

    setFilesToDownload(notification, shouldNotify, 1);

    let data = {
        from: settings.data.username,
        to: notification.email,
        subject: 'LogAnalyzer Notification',
        template: 'notification',
        context: {
            files: notification.files.filter(file => file.toSend == 1)
        }
    };

    return new Promise((resolve, reject) => {
        transporter.sendMail(data, (err, info) => {
            if (err) {
                reject(err);
                schedule.scheduleJob(moment().add(settings.data.interval, "minutes"), function() {
                    sendMail(notification, shouldNotify);
                });
            }
            setFilesToDownload(notification, shouldNotify, 2);
            resolve(info);
        });
    });
}

// Sets flag within files which inform about status ( nothing / to send / sent )
function setFilesToDownload(notification, shouldNotify, toSend) {
    shouldNotify.forEach((notify, index) => {
        if (notify) {
            notification.requiredVariablesSets[index].forEach(variable => {
                notification.files.filter(file => equalVariables(variable, file.variable) && isSendToChange(file, toSend)).forEach(file => {
                    file.toSend = toSend;
                });
            });
        }
    });
}

// Checks whether variables are equal
function equalVariables(variable1, variable2) {
    return variable1.name == variable2.name && variable1.value == variable2.value;
}

// Change flag which inform about status ( nothing / to send / sent )
function isSendToChange(file, toSend) {
    return ((file.toSend == 1 && toSend == 2) || (file.toSend == 0 && toSend == 1));
}