let CONFIG = require('../config');
var mongoose = require('mongoose');
var fs = require('fs');
var glob = require('glob');
var zlib = require('zlib');
var Ssh = new(require('node-ssh'))();
var ftp = new(require('easy-ftp'))();
var request = require('request');
const pathModule = require('path');
const csvtojson = require('csvtojson');
var Host = require('../models/host.model');
var Log = require('../models/log.model');


module.exports = {
    downloadLogsForNotification
};

/**
 * This function downloads logs for one host
 * @param {String} hostId host's id  for which logs should be download
 */
function downloadLogsForNotification(hostId) {
    return new Promise((resolve, reject) => {
        Host.findOne({ _id: hostId }, (err, host) => {
            if (err) throw err;
            host.protocolType = host.protocolType.toLowerCase();

            if (host.protocolType == "ftp") {
                console.log("ftp start");
                var config = createConnectionConfigFor(host);
                ftp.connect(config);

                downloadFileFtp(host).then(files => {
                        ftp.close();
                        Promise.all(addFilesToDB(files, host)).then(result => resolve(result));
                    },
                    err => {
                        console.log(err);
                        reject({ error: err });
                    });
            } else if (host.protocolType == "ssh") {
                console.log("ssh start");
                downloadLogsBySSH(host).then(files => {
                        Promise.all(addFilesToDB(files, host)).then(result => resolve(result));
                    },
                    err => {
                        console.log(err);
                        reject({ error: err });
                    });

            } else if (host.protocolType == "http") {
                console.log("hhtp start");
                Promise.all(downloadLogsByHTTP(host)).then(
                    data => {
                        resolve(data);
                    },
                    err => {
                        return { error: err };
                    }
                );
            }
        });
    });
}

// Downloads logs by protocol SSH
async function downloadLogsBySSH(host) {
    let config = createConfigForSsh(host);

    var allFilesToDownload = [];
    let promise = new Promise((resolve, reject) => {

        Ssh.connect(config).then(function() {
            host.paths.forEach((path, i) => {

                getPWDPath(path.path).then(
                    fullPath => {
                        getFilesList(path.path).then(files => {
                            getDateOfTheLastFile(host._id, path.path).then(dateOfTheLastFile => {
                                files.forEach(file => {
                                    prepareFileBeforeSave(file, host, path);
                                });
                                var filesToDownload = files.filter(isNew(dateOfTheLastFile));
                                allFilesToDownload.push(...filesToDownload);
                                downloadSSHFiles(fullPath, filesToDownload, host, resolve, reject).then(() => {
                                    if (i == host.paths.length - 1)
                                        resolve("ok");
                                });
                            });
                        });
                    },
                    err => {
                        console.log(err);
                    }
                );
            });
        });
    });

    await promise;
    return allFilesToDownload;
}

// Downloads logs by protocol FTP
async function downloadFileFtp(host) {

    var allFilesToDownload = [];
    var promise = new Promise(function(resolve, reject) {

        Promise.all(completeArrayToDownloadFTP(host)).then(arrayToDownload => {
            arrayToDownload = [].concat.apply([], arrayToDownload);

            arrayToDownload.forEach(entry => {
                allFilesToDownload.push(entry.file);
            });

            ftp.download(arrayToDownload, function(err, files) {
                if (err) {
                    console.log(err);
                    reject(err);
                }
                resolve("ok");
            });
        });
    });

    await promise;
    return allFilesToDownload;
}

// Downloads logs by protocol HTTP
function downloadLogsByHTTP(host) {
    var promises = [];
    var files = host.paths;
    host.paths.forEach(path => {
        var pathWithoutSlashes = path.path.replace(/\//g, "_");
        promises.push(new Promise((resolve, reject) => {
            let fullPath = host.address + path.path;
            let newName = host.address.replace("http://", "").replace("https://", "") + '_' + pathWithoutSlashes;
            console.log("ADDRESS: ", fullPath);
            request.get(fullPath, {
                'auth': {
                    'user': host.login,
                    'pass': host.password,
                    'sendImmediately': false
                }
            }).on('error', function(err) {
                reject(err);
            }).pipe(fs.createWriteStream(CONFIG.filesDownloadedSavingPath + '/' + newName)
                .on('finish', function(data) {
                    resolve("ok");
                })
            );
        }));
    });

    return promises;
}

// Completes array of files which should be download by FTP
function completeArrayToDownloadFTP(host) {

    var arrayToDownload = [];
    var allFilesToDownload = [];
    var promises = [];

    host.paths.forEach((path) => {

        promises.push(new Promise((resolve, reject) => {

            ftp.ls(path.path, (err, list) => {
                if (err) console.log(err);

                getDateOfTheLastFile(host._id, path.path).then(dateOfTheLastFile => {

                    var filesToDownload = list.filter(isNew(dateOfTheLastFile));
                    allFilesToDownload.push(...filesToDownload);

                    filesToDownload.forEach((file) => {
                        prepareFileBeforeSave(file, host, path);
                        arrayToDownload.push({
                            remote: pathModule.join(path.path, file.name),
                            local: CONFIG.filesDownloadedSavingPath + "/" + host.address + "_" + host.login + "_" + file.name,
                            file: file
                        });
                    });
                    resolve(arrayToDownload);
                    arrayToDownload = [];
                });
            });
        }));
    });
    return promises;
}

// Saves logs downloaded by HTTP in database
function saveHttpInDB(host, prefix, path) {
    console.log("saveHttpInDB");
    return new Promise((resolve, reject) => {
        path.path = path.path.replace(/\//g, "_");
        Log.findOne({ hostId: host._id, filename: pathModule.basename(path.path) }, (err, fileFromDB) => {
            if (err) throw err;
            var newLog = {
                filename: path.path,
                hostId: host._id,
                update_at: new Date(),
                last_modified: new Date(),
                timePattern: {
                    pattern: path.pattern,
                    datetimeFormat: path.datetimeFormat
                }
            };

            if (fileFromDB) {
                newLog.visitedLinesNumber = countLinesInFile(fileFromDB);
                newLog._id = fileFromDB._id;
                fileFromDB.remove();
            }

            resolve(saveLog(prefix + path.path, newLog));
        });
    });

}

// Return default port for protocol type
function getDefaultPortFor(protocolType) {
    return CONFIG.ports[protocolType];
}

// Creates config for connection
function createConnectionConfigFor(host) {
    return {
        host: host.address,
        username: host.login,
        password: host.password,
        port: host.port ? host.port : getDefaultPortFor(host.protocolType)
    };
}

// Adds files to database
function addFilesToDB(files, host) {
    let filesPromises = [];
    files.forEach((file) => {
        file.name = file.name ? file.name : file.filename;
        var prefix = pathModule.join(CONFIG.filesDownloadedSavingPath, host.address + "_" + host.login + "_");
        var patt = new RegExp(".gz");
        if (patt.test(file.filename)) {
            filesPromises.push(gunzipAndSave(host, file, prefix + file.name));
        } else {
            filesPromises.push(saveInDB(host, file, prefix + file.name));
        }
    });
    return filesPromises;
}

// Save log to database
function save(log) {
    return new Promise((resolve, reject) => {
        let newLog = new Log(log);
        newLog.save((err, _log) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(_log);
        });
    });
}

// Gunzip archive file
function gunzip(filename) {
    return new Promise((resolve, reject) => {
        var newFilename = filename.replace('.gz', '');
        var writestream = fs.createWriteStream(newFilename);
        var stream = fs.createReadStream(filename).pipe(zlib.createGunzip()).pipe(writestream);
        stream.on('finish', (err, data) => {
            if (err) console.log(err);
            resolve("ok");
        });
    });
}

// Gunzip archive file and save in database
function gunzipAndSave(host, file, name) {
    return new Promise((resolve, reject) => {
        gunzip(name).then(() => {
            var newname = name.replace('.gz', '');
            console.log("Unzip. New filename: " + newname);
            saveInDB(host, file, newname).then(file => {
                removeFile(name);
                resolve(file);
            });
        });
    });
}

// Saves log in database based on file's details
function saveInDB(host, file, filename) {

    return new Promise((resolve, reject) => {
        console.log("try save file: ", file);
        var name = file.name ? file.name : file.filename;
        Log.findOne({ filename: name, hostId: host._id }, (err, log) => {
            if (err) throw err;

            var newLog = {
                filename: name,
                path: file.path,
                hostId: host._id,
                size: file.size,
                last_modified: file.date,
                update_at: new Date(),
                timePattern: file.timePattern,
            };

            if (log) {
                newLog._id = log._id;
                newLog.visitedLinesNumber = countLinesInFile(log);
                log.remove();
                console.log("Deleted log (id = " + log._id + ")");
            }
            console.log("FILENAME: ", filename);
            resolve(saveLog(filename, newLog));
        });
    });

}

// Saves log in database based on filename
function saveLog(filename, log) {
    return new Promise((resolve, reject) => {
        fs.readFile(filename, 'utf8', function(err, content) {
            if (err) {
                reject(err);
                throw err;
            }
            log.content = content;

            save(log).then(() => {
                console.log('successfully added to DB: ' + filename);
                removeFile(filename);
                resolve(log);
            });
        });
    });
}

// Count number of lines in file
function countLinesInFile(file) {
    var arrayOfLines = file.content.split("\n");
    return arrayOfLines.length;
}

// Removes file from disc
function removeFile(filename) {
    glob(filename, (err, files) => {
        if (err) throw err;
        files.forEach(file => {
            fs.unlink(file, (err) => {
                if (err) throw err;
            });
        });
    });
}

// Sets file's info befeore saving to database
function prepareFileBeforeSave(file, host, path) {
    file.path = path.path;
    file.timePattern = {
        pattern: path.pattern,
        datetimeFormat: path.datetimeFormat
    };
    file.date = host.protocolType == "ssh" ? new Date(file.date + " " + file.time) : new Date(file.time);
    file.hostId = host.id;

    delete file.owner;
    delete file.userPermissions;
    delete file.groupPermissions;
    delete file.otherPermissions;
    delete file.type;
    delete file.time;
}

// Creates config for connection by SSH
function createConfigForSsh(host) {
    return {
        host: host.address,
        username: host.login,
        port: host.port || 22,
        password: host.password,
        tryKeyboard: true,
        onKeyboardInteractive: (name, instructions, instructionsLang, prompts, finish) => {
            if (prompts.length > 0 && prompts[0].prompt.toLowerCase().includes('password')) {
                finish([host.password]);
            }
        }
    };
}

// Gets current path while SSH connection
function getPWDPath(path) {
    return new Promise((resolve, reject) => {
        Ssh.execCommand('cd ' + path + '; pwd', { cwd: path }).then(
            res => {
                resolve(res.stdout);
            },
            err => {
                reject(err);
            });
    });
}

// Gets list of files in current directory while SSH connection
function getFilesList(path) {
    return new Promise((resolve, reject) => {
        Ssh.execCommand('ls -l -D --time-style=\'+%Y-%m-%d %H:%M:%S\' ' + path + ' *.log | awk  \'BEGIN{OFS = ";"};{print $8,$5, $6, $7}\'', { cwd: path }).then(function(result) {
            convertFileDataToJson(result.stdout).then(files => {
                console.log("files number: ", files.length);
                resolve(files);
            }, err => {
                reject(err);
            });
        });
    });

}

// Downloads files by SSH
async function downloadSSHFiles(path, files, host, resolve, reject) {
    for (var i = 0; i < files.length; i++) {
        try {
            var prefix = CONFIG.filesDownloadedSavingPath + '/' + host.address + "_" + host.login + "_";
            let destinationPath = pathModule.join(CONFIG.filesDownloadedSavingPath, host.address + "_" + host.login + "_" + files[i].filename);
            let sourcePath = pathModule.join(path, files[i].filename);
            console.log(destinationPath + " : " + sourcePath);
            await Ssh.getFile(destinationPath, sourcePath).then(
                (Contents) => {},
                (err) => {
                    console.log(err);
                }
            );
        } catch (e) {
            console.error(e);
        }
    }
    return files;
}

// Create JSON object with file details
function convertFileDataToJson(data) {
    console.log("data ", data);
    let files = [];
    let config = {
        delimiter: ';',
        headers: ["filename", "size", "date", "time"]
    };
    return new Promise((resolve, reject) => {
        csvtojson(config).fromString(data).on('data', (csvRow) => {
            files.push(JSON.parse(csvRow.toString('utf8')));
        }).on('done', () => {
            console.log("after csv: ", files);
            resolve(files);
        }).on('error', (error) => {
            reject(error);
        });
    });
}

// Finds date of the newest file for specified host and path
function getDateOfTheLastFile(hostId, path) {
    let query = Log.find({ hostId: hostId, path: path }).sort({ last_modified: 'desc' });
    return new Promise((resolve, reject) => {
        query.exec((err, logs) => {
            if (err) {
                reject(err);
                return;
            }
            if (logs.length > 0) {
                resolve(logs[0].last_modified);
            } else {
                resolve(new Date(1900, 1));
            }
        });
    });
}

// Checks whether is new (doesn't exists in database)
function isNew(dateOfTheLastFile) {
    return function(file) {
        if (String(file.date).indexOf("Invalid Date") != -1) {
            return false;
        }
        var isNew = file.date > dateOfTheLastFile;
        if (file.name) {
            return file.name.indexOf(".log") != -1 && isNew;
        } else {
            return file.filename.indexOf(".log") != -1 && isNew;
        }
    };
}