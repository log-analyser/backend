var express = require('express');
var app = express();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var router = require('./router');
var http = require('http').Server(app);
var config = require('./config');
var winston = require('winston');

const logger = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    transports: [
        //
        // - Write to all logs with level `info` and below to `combined.log` 
        // - Write all logs error (and below) to `error.log`.
        //
        new winston.transports.File({ filename: 'error.log', level: 'error' }),
        new winston.transports.File({ filename: 'combined.log' })
    ]
});

if (process.env.NODE_ENV !== 'production') {
    logger.add(new winston.transports.Console({
        format: winston.format.simple()
    }));

    console.log = function(content) {
        logger.info(content);
    }

    console.error = function(content) {
        logger.error(content);
    }
}

mongoose.connect(config.databaseURL).then(
    () => { console.log("Connected to database..."); },
    (error) => {
        console.error("Error while trying to connect database...");
        console.error(error)
    }
);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", config.frontendURL);
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Credentials', true);
    next();
});
app.use(router);


var db = mongoose.connection;

http.listen(config.port, _ => {
    console.log("Server is running on 5000");
});

module.exports = http