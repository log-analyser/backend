var express = require('express');
var router = express.Router();
var hostService = require('./services/host.service');
var logService = require('./services/log.service');
var rulesSetService = require('./services/rulesSet.service');
var notificationService = require('./services/notification.service');
var settingService = require('./services/setting.service');

module.exports = router;

router.get('/hosts', hostService.getHosts);
router.post('/host', hostService.postHost);
router.get('/hosts/:id', hostService.getHost);
router.put('/host/:id', hostService.putHost);
router.delete('/host/:id', hostService.deleteHost);
router.get('/hosts/:id/files', hostService.deleteFiles);

router.get('/logs/count', logService.getLogsCount);
router.get('/logs/:hostId', logService.downloadLogs);
router.get('/logs', logService.getLogsBasicData);
router.get('/logs/:id/content', logService.getLogContent);
router.delete('/log/:id', logService.deleteLog);
router.delete('/logs', logService.deleteLogs);

router.post('/rulesset', rulesSetService.postRulesSet);
router.get('/rulessets', rulesSetService.getRulesSets);
router.delete('/rulesset/:id', rulesSetService.deleteRulesSet);
router.put('/rulesset/:id', rulesSetService.putRulesSet);
router.get('/rulessets/:hostId', rulesSetService.getRulesSetsByHostId);
router.get('/rulesset/:id', rulesSetService.getRulesSet);
router.delete('/rulesset/:id', rulesSetService.deleteRulesSet);

router.get('/notifications', notificationService.getNotifications);
router.post('/notification', notificationService.postNotification);
router.get('/notifications/:id', notificationService.getNotification);
router.put('/notification/:id', notificationService.putNotification);
router.delete('/notification/:id', notificationService.deleteNotification);
router.get('/notification/:id/run', notificationService.runNotification);
router.get('/notification/:id/schedule', notificationService.scheduleNotification);
router.get('/notification/:id/stop', notificationService.stopScheduledNotification);

router.get('/settings', settingService.getSettings);
router.post('/setting', settingService.postSetting);
router.get('/settings/:id', settingService.getSetting);
router.put('/setting/:id', settingService.putSetting);
router.delete('/setting/:id', settingService.deleteSetting);